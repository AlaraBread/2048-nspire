#include <libndls.h>
#include <ngc.h>
#include <stdlib.h>
#include <math.h>
#define BOARD_SIZE 4
#define TOP_BAR_SIZE 16
#define BUF_SIZE 30

typedef struct RgbColor
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
} RgbColor;

typedef struct HsvColor
{
	unsigned char h;
	unsigned char s;
	unsigned char v;
} HsvColor;

RgbColor HsvToRgb(HsvColor hsv)
{
	RgbColor rgb;
	unsigned char region, remainder, p, q, t;
	
	if (hsv.s == 0)
	{
		rgb.r = hsv.v;
		rgb.g = hsv.v;
		rgb.b = hsv.v;
		return rgb;
	}
	
	region = hsv.h / 43;
	remainder = (hsv.h - (region * 43)) * 6; 
	
	p = (hsv.v * (255 - hsv.s)) >> 8;
	q = (hsv.v * (255 - ((hsv.s * remainder) >> 8))) >> 8;
	t = (hsv.v * (255 - ((hsv.s * (255 - remainder)) >> 8))) >> 8;
	switch (region)
	{
		case 0:
			rgb.r = hsv.v; rgb.g = t; rgb.b = p;
			break;
		case 1:
			rgb.r = q; rgb.g = hsv.v; rgb.b = p;
			break;
		case 2:
			rgb.r = p; rgb.g = hsv.v; rgb.b = t;
			break;
		case 3:
			rgb.r = p; rgb.g = q; rgb.b = hsv.v;
			break;
		case 4:
			rgb.r = t; rgb.g = p; rgb.b = hsv.v;
			break;
		default:
			rgb.r = hsv.v; rgb.g = p; rgb.b = q;
			break;
	}
	
	return rgb;
}

//applies gravity in the passed direction
//directions are {up down left right}
void shiftGrid(int grid[BOARD_SIZE][BOARD_SIZE], int direction){
	for(int i = 0; i < BOARD_SIZE; i++){
		switch(direction){
			case 0:
				for(int x = 0; x < BOARD_SIZE; x++){
					for(int y = 0; y < BOARD_SIZE-1; y++){
						if(grid[x][y] == 0 && grid[x][y+1] != 0){
							// move tiles down
							grid[x][y] = grid[x][y+1];
							grid[x][y+1] = 0;
						}else if(grid[x][y] == grid[x][y+1]){
							//merge tiles
							grid[x][y] *= 2;
							grid[x][y+1] = 0;
						}
					}
				}
				break;
			case 1:
				for(int x = 0; x < BOARD_SIZE; x++){
					for(int y = BOARD_SIZE-1; y > 0; y--){
						if(grid[x][y] == 0 && grid[x][y-1] != 0){
							//move tiles down
							grid[x][y] = grid[x][y-1];
							grid[x][y-1] = 0;
						}else if(grid[x][y] == grid[x][y-1]){
							//merge tiles
							grid[x][y] *= 2;
							grid[x][y-1] = 0;
						}
					}
				}
				break;
			case 2:
				for(int x = 0; x < BOARD_SIZE-1; x++){
					for(int y = 0; y < BOARD_SIZE; y++){
						if(grid[x][y] == 0 && grid[x+1][y] != 0){
							//move tiles down
							grid[x][y] = grid[x+1][y];
							grid[x+1][y] = 0;
						}else if(grid[x][y] == grid[x+1][y]){
							//merge tiles
							grid[x][y] *= 2;
							grid[x+1][y] = 0;
						}
					}
				}
				break;
			case 3:
				for(int x = BOARD_SIZE-1; x > 0; x--){
					for(int y = 0; y < BOARD_SIZE; y++){
						if(grid[x][y] == 0 && grid[x-1][y] != 0){
							//move tiles down
							grid[x][y] = grid[x-1][y];
							grid[x-1][y] = 0;
						}else if(grid[x][y] == grid[x-1][y]){
							//merge tiles
							grid[x][y] *= 2;
							grid[x-1][y] = 0;
						}	
					}
				}
				break;
		}
	}
}

//adds a random tile to the passed grid
int addRandom(int grid[BOARD_SIZE][BOARD_SIZE]){
	int count = 0;
	for(int x = 0; x < BOARD_SIZE; x++){
		for(int y = 0; y < BOARD_SIZE; y++){
			if(grid[x][y] == 0){
				count += 1;
			}
		}
	}
	if(count == 0){
		return 0;
	}
	int r = rand()%count;
	for(int x = 0; x < BOARD_SIZE; x++){
		for(int y = 0; y < BOARD_SIZE; y++){
			if(grid[x][y] == 0){
				r -= 1;
				if(r <= 0){
					if(rand()%3==0){
						grid[x][y] = 4;
					}else{
						grid[x][y] = 2;
					}
					return 1;
				}
			}
		}
	}
	return 1;
}

int readNumFromFile(char* filename){
	FILE *f = fopen(filename, "r");
	if(f == NULL){
		f = fopen(filename, "w");
		fprintf(f, "0");
		fclose(f);
		return 0;
	}
	int out;
	
	fscanf(f, "%d", &out);
	fclose(f);

	return out;
}

void writeNumToFile(char* filename, int num){
	FILE *f = fopen(filename, "w");
	fprintf(f, "%d", num);
	fclose(f);
}

int main(int _argc, char* argv[]){
	
	enable_relative_paths(argv);
	
	//read seed from file
	FILE *f = fopen("seed.tns", "r");
	if(f == NULL){
		f = fopen("seed.tns", "w");
		fprintf(f, "0");
		fclose(f);
		
		f = fopen("seed.tns", "r");
	}
	int seed;
	fscanf(f, "%d", &seed);
	fclose(f);
	
	seed += 1;
	
	f = fopen("seed.tns", "w");
	fprintf(f, "%d", seed);
	fclose(f);
	
	srand(seed);
	
	int grid[BOARD_SIZE][BOARD_SIZE];
	for(int x = 0; x < BOARD_SIZE; x++){
		for(int y = 0; y < BOARD_SIZE; y++){
			grid[x][y] = 0;
		}
	}
	
	addRandom(grid);
	
	Gc gc = gui_gc_global_GC();
	gui_gc_setRegion(gc, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	gui_gc_begin(gc);
	
	
	char str[BUF_SIZE];
	char out[BUF_SIZE];
	int highScore = readNumFromFile("2048highScore.tns");

	while(1){
		int okay = 1;
		if(isKeyPressed(KEY_NSPIRE_UP)){
			shiftGrid(grid, 0);
			okay = addRandom(grid);
			while(isKeyPressed(KEY_NSPIRE_UP));
		}
		if(isKeyPressed(KEY_NSPIRE_DOWN)){
			shiftGrid(grid, 1);
			okay = addRandom(grid);
			while(isKeyPressed(KEY_NSPIRE_DOWN));
		}
		if(isKeyPressed(KEY_NSPIRE_LEFT)){
			shiftGrid(grid, 2);
			okay = addRandom(grid);
			while(isKeyPressed(KEY_NSPIRE_LEFT));
		}
		if(isKeyPressed(KEY_NSPIRE_RIGHT)){
			shiftGrid(grid, 3);
			okay = addRandom(grid);
			while(isKeyPressed(KEY_NSPIRE_RIGHT));
		}
		if(isKeyPressed(KEY_NSPIRE_ESC)){
			break;
		}
		int score = 0;
		for(int x = 0; x < BOARD_SIZE; x++){
			for(int y = 0; y < BOARD_SIZE; y++){
				score = score + grid[x][y];
			}
		}
		if(score > highScore){
			highScore = score;
		}
		if(!okay || isKeyPressed(KEY_NSPIRE_R)){
			for(int x = 0; x < BOARD_SIZE; x++){
				for(int y = 0; y < BOARD_SIZE; y++){
					grid[x][y] = 0;
				}
			}
			score = 0;
			if(isKeyPressed(KEY_NSPIRE_R)){
				while(isKeyPressed(KEY_NSPIRE_R));
			}
		}
		gui_gc_setColorRGB(gc, 0, 0, 0);
		gui_gc_fillRect(gc, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		gui_gc_setFont(gc, SerifBold24);
		for(int x = 0; x < BOARD_SIZE; x++){
			for(int y = 0; y < BOARD_SIZE; y++){
				//bg color
				if(grid[x][y] != 0){
					HsvColor hsv;
					hsv.h = (int)(20*((log((double)grid[x][y])/0.69314718056)-1))%255;
					if(grid[x][y] == 64){
						hsv.h += 10;
					}
					hsv.s = 255;
					hsv.v = 200;
					RgbColor rgb = HsvToRgb(hsv);
					
					gui_gc_setColorRGB(gc, rgb.r, rgb.g, rgb.b);
				}else{
					gui_gc_setColorRGB(gc, 255, 255, 255);
				}
				gui_gc_fillRect(gc, x*(SCREEN_WIDTH/BOARD_SIZE), y*((SCREEN_HEIGHT-TOP_BAR_SIZE)/BOARD_SIZE)+TOP_BAR_SIZE, SCREEN_WIDTH/BOARD_SIZE, (SCREEN_HEIGHT-TOP_BAR_SIZE)/BOARD_SIZE);
				//text
				if(grid[x][y] != 0){
					sprintf(str, "%d", grid[x][y]);
				}else{
					sprintf(str, " ");
				}
				ascii2utf16(out, str, BUF_SIZE);
				gui_gc_setColorRGB(gc, 255, 255, 255);
				gui_gc_drawString(gc, out, 
						x*(SCREEN_WIDTH/BOARD_SIZE)+(5), 
						y*((SCREEN_HEIGHT-TOP_BAR_SIZE)/BOARD_SIZE)+32,
						GC_SM_NORMAL | GC_SM_MIDDLE);
			}
		}
		gui_gc_setFont(gc, SerifBold9);
		// draw score
		sprintf(str, "Score: %d", score);
		ascii2utf16(out, str, BUF_SIZE);
		gui_gc_drawString(gc, out, 0, 0, GC_SM_NORMAL | GC_SM_TOP);
		//draw high score
		sprintf(str, "High Score: %d", highScore);
		ascii2utf16(out, str, BUF_SIZE);
		int len = 0;
		while(str[len] != '\0'){
			len++;
		}
		gui_gc_drawString(gc, out, SCREEN_WIDTH-(gui_gc_getStringWidth(gc, SerifBold9, out, 0, len)), 0, GC_SM_NORMAL | GC_SM_TOP);
		
		gui_gc_blit_to_screen(gc);
		idle();
		wait_key_pressed();
	}
	writeNumToFile("2048highScore.tns", highScore);
	gui_gc_finish(gc);
}
